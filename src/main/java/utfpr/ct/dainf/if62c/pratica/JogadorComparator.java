/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.ArrayList;
import java.util.Comparator;

/**
 *
 * @author Joao
 */
public class JogadorComparator implements Comparator<Jogador> {
    ArrayList<Jogador> timenome = new ArrayList<>(11); 
    ArrayList<Jogador> timenumero = new ArrayList<>(11); 
    int codigo; 
    boolean a1, a2, a3;

    public JogadorComparator(boolean a1, boolean a2, boolean a3) {
        this.a1 = a1;
        this.a2 = a2;
        this.a3 = a3;
    }
    
    public JogadorComparator() {
        this.a1 = true;
        this.a2 = true;
        this.a3 = true;
    }    

    @Override
    public int compare(Jogador o1, Jogador o2) {
        int v=0;
        if (a1){
            Integer o1n = o1.numero;
            Integer o2n = o2.numero;
            if (a2){
                v = o1n.compareTo(o2n);
            }
            else{
                v = o2n.compareTo(o1n);
            }                
            if (v==0){
                if (a3)
                    v = o1.nome.compareTo(o2.nome);
                else
                    v = o2.nome.compareTo(o1.nome);
            }
        }
        else{
            if (a3)
                v = o1.nome.compareTo(o2.nome);
            else
                v = o2.nome.compareTo(o1.nome);
            if (v==0){
                Integer o1n = o1.numero;
                Integer o2n = o2.numero;
                if (a2){
                    v = o1n.compareTo(o2n);
                }
                else{
                    v = o2n.compareTo(o1n);
                } 
            }
        }
        return v;
    }
}
/*
0: nome_a 
1: numero_a 
2: nome_a, numero_a 
3: nome_a numero_d 
4: nome_d
5: numero_d
6: numero_a nome_d
7: numero_d nome_d
*/