package utfpr.ct.dainf.if62c.pratica;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Time {
    private HashMap<String, Jogador> jogadores = new HashMap<>();

    public HashMap<String, Jogador> getJogadores() {
        return jogadores;
    }
    
    public void addJogador(String posicao, Jogador asdf) {
        this.jogadores.put(posicao, asdf);
    }
    
    public List<Jogador> ordena(JogadorComparator a) {
        List<Jogador> lista = new ArrayList<>(jogadores.size());
        for(String n:getJogadores().keySet())
            lista.add(new Jogador(getJogadores().get(n).numero, getJogadores().get(n).nome));
        return lista;
    }
    
}
